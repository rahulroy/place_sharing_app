# README

Social place sharing app.

Tasks:
* Share a location with other people. -- done

* You would be asked to login and after logging in you can. -- done

* Places you have shared. -- done

* Places shared with you. -- done

* Show location on the map. -- done

* Share location either as public, or with selected people. -- done

* Anyone can view your public shared places by going to your page (<site>/<username>. -- done

* You can use Seed file to create Users and set up their Friends relationship. -- done


## Notes

I've tried my best:
* to validations where required

* to implement everything that was highlighted in the task and them some more.

* to make sure that the unused code is not there.

* to add an index to all the column which required sorting & filtering.

* used counter cache where it could be added - users posts

* tried to organize my javascript well - The technique used http://brandonhilkert.com/blog/organizing-javascript-in-rails-application-with-turbolinks/

* Tried showing alerts & notices where required for good UX.

* to reuse the code as much as possible with the help of features like partials, filters, scope, view helpers, class methods, instance methods, etc.

* Tried keeping controller as slim as possible and moving things to model

* to follow standard style guides like this https://github.com/bbatsov/rails-style-guide

* to make sure there are no N+1 query or unused eager loading.

App Flow & Features:
The app is pre-populated with places data. When a user signs up they get redirected to the feed page. There they can see their own posts as well as posts shared with them. Here they can publish a post too by typing location, location gets autocompleted using easy-autocomplete & pg_search plugins. The user can select a location from the list and add a line or two about the location and finally use a toggle button to make a post either public(default) or private. If they choose to make it private, then user list appears they can use ctrl + click to select multiple users to share with[1][2]. At any time users can click on a post to view the post page where can see the **location on the map**.

If a user visits someone's profile, then can they only see public posts and the posts shared with them.


Choices:
A post is shared with other used: Here I'm using Postgres array feature to store **user_ids** and query against posts table to show posts 'shared with you'.
using 'gon' gem to share pass variables to javascript.



Missed out:
On user feed page, there are two kinds of posts self & shared with you. Ideally, there should have been a visual indicator to separate the two.
Would have added database constraints if had more time
Make private post URL unguessable.


Extension left out as it wasn't part of the assignment plus time constraint:
[1] here auto-complete feature could have been used
[2] here a friendship model could be introduced to make 'share with' users list more relevant.
