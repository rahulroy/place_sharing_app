class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, presence: true, uniqueness: true
  validates :name, presence: true

  has_many :posts


  def self.search_with_params(params)
    users = self.all
    users = users.where.not(id: params[:all_users]) if params[:all_users].present?
  end

  def public_posts
    self.posts.where(public: true)
  end

  def viewable_posts(user)
    user.posts.search_with_params(user_ids: id) + user.public_posts
  end

  def feed_posts
    Post.includes(:place).search_with_params(user_ids: id) + posts.includes(:place)
  end
end
