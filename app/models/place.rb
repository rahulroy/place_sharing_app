class Place < ApplicationRecord
  include PgSearch
  pg_search_scope :match_by_name, against: :name, using: { tsearch: { prefix: true } }

  validates :name, :lat, :long, presence: true

  def self.search_with_params(params)
    places = self.all
    places = places.where("places.name LIKE ?", "%#{params[:name]}%") if params[:name].present?
  end
end
