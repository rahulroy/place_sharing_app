class Post < ApplicationRecord
  attr_accessor :placename

  include PgSearch
  pg_search_scope :match_by_content, against: :content, using: { tsearch: { prefix: true } }

  validates :content, presence: true

  belongs_to :place
  belongs_to :user, counter_cache: true

  scope :public_posts, -> { where(public: true) }

  def self.search_with_params(params)
    posts = all
    posts = posts.where("':user_ids' = ANY (user_ids)", user_ids: params[:user_ids]) if params[:user_ids].present?
  end

  def can_be_viewed?(user)
    user_ids.include?(user.id) || user.posts.include?(self)
  end

  def is_private?
    public == false
  end
end
