module SharedHelper
  def post_privacy_icon(post)
    post.public ? 'globe' : 'lock'
  end

  def non_owner_class(post)
    'post_shared_with_you' if post.user != current_user
  end

  def post_privacy_tooltip(post)
    post.public ? 'Public post' : 'Private post'
  end
end
