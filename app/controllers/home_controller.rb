class HomeController < ApplicationController
  before_action :authenticate_user!, only: [:landing]

  def landing
    @users = User.search_with_params(all_users: current_user.id).pluck(:name, :id).to_h
    @post = Post.new
    @posts = current_user.feed_posts.paginate(page: params[:page])
  end

  def front
    @post = Post.includes(:place).public_posts.last
    (gon.place = @post.place) if @post
  end
end
