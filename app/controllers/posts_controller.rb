class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :update_params, only: [:create]
  before_action :check_location, only: [:create]
  before_action :set_post, only: [:show]
  before_action :check_privacy, only: [:show]

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to post_path(@post), notice: 'Post save successfully!'
    else
      redirect_to root_path, alert: @post.errors
    end
  end

  def show
    gon.place = @post.place
  end

  def shared
    params[:user_ids] = current_user.id
    @posts = Post.search_with_params(params)
  end

  private

  def post_params
    params.require(:post).permit(:content, :place_id, :public, user_ids: [])
  end

  def check_location
    @place = Place.find_by(name: params[:post][:placename])
    if @place
      params[:post][:place_id] = @place.id
    else
      redirect_to places_path, alert: 'Please select one of these location.'
    end
  end

  def update_params
    params[:post][:user_ids] = params[:post][:user_ids].reject(&:empty?)
  end

  def set_post
    @post = Post.find(params[:id]) if params[:id]
  end

  def check_privacy
    if @post.is_private?
      render_404 && return unless @post.can_be_viewed?(current_user)
    end
  end
end
