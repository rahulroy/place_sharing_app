class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :check_username, only: [:profile]

  def profile
    @users = User.search_with_params(all_users: current_user.id).pluck(:name, :id).to_h
    @post = Post.new
    if current_user.username == params[:username]
      @posts = current_user.posts.includes(:place).paginate(page: params[:page])
    else
      @posts = current_user.viewable_posts(@user).paginate(page: params[:page])
    end
  end

  private

  def check_username
    @user = User.find_by(username: params[:username])
    render_404 && return unless @user
  end
end
