class PlacesController < ApplicationController
  before_action :authenticate_user!

  def index
    @places = Place.paginate(page: params[:page])
  end

  def show
    @place = Place.find(params[:id])
    gon.place = @place
  end

  def search
    @places = Place.match_by_name(params[:name]).limit(5).pluck(:name)
    render json: @places
  end
end
