$('.home.landing').ready(function() {
  $('.hidden_text').hide();
  $('.post_share_state').tooltip();

  var options = {
    url: function(name) {
      return "/places/search.json?name=" + name;
    },

    requestDelay: 500
  };

  $("#post_placename").easyAutocomplete(options);

  $(function() {
    $('#post_public').change(function() {
      if (event.toElement.className === "btn btn-primary toggle-on") {
        $('.hidden_text').show();
      } else {
        $('.hidden_text').hide();
      }
    })
  });
});
