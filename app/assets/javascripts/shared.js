$(document).ready(function() {
  $(".alert.alert-success").fadeTo(2000, 500).slideUp(500);

  $(".alert.alert-danger").fadeTo(2000, 500).slideUp(500);
})
