$('.home.front').ready(function() {
  var map = new ol.Map({
    target: 'front_page_map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([parseFloat(gon.place.long), parseFloat(gon.place.lat)]),
      zoom: 8
    })
  });
});
