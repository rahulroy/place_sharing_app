$('.users.profile').ready(function() {
  $('.hidden_text').hide();
  $('.post_share_state').tooltip();

  // TODO: Remove this
  var options = {
    url: "/places/search.json"
  };

  $("#post_placename").easyAutocomplete(options);

  $(function() {
    $('#post_public').change(function() {
      if (event.toElement.className === "btn btn-primary toggle-on") {
        $('.hidden_text').show();
      } else {
        $('.hidden_text').hide();
      }
    })
  });
});
