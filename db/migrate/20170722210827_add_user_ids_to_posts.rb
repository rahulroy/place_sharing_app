class AddUserIdsToPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :user_ids, :integer, array: true
    add_index :posts, :user_ids, using: 'gin'
  end
end
