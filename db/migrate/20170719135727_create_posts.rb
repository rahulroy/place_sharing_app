class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :name
      t.integer :lat
      t.integer :long
      t.integer :user_id

      t.timestamps
    end
    add_index :posts, :user_id
  end
end
