class UpdatePostsTable < ActiveRecord::Migration[5.1]
  def change
    remove_column :posts, :lat
    remove_column :posts, :long
    remove_column :posts, :name
    add_column :posts, :content, :text
  end
end
