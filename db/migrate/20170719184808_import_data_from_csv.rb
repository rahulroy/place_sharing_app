class ImportDataFromCsv < ActiveRecord::Migration[5.1]
  def up
    require 'csv'
    csv_text = File.read('db/data/cities.csv')
    csv = CSV.parse(csv_text, headers: true)
    csv.each do |row|
      p = Place.new(name: row['name'], lat: row['lat'].to_f, long: row['long'].to_f)
      p.save
    end
  end

  def down
    Place.destroy_all
  end
end
