Rails.application.routes.draw do
  devise_for :users

  authenticated :user do
    root to: 'home#landing'
  end

  unauthenticated :user do
    root to: 'home#front'
  end

  resources :places, only: [:index, :show] do
    collection do
      get 'search'
    end
  end

  resources :posts, only: [:create, :show] do
    collection do
      get 'shared'
    end
  end

  resources :users do
    collection do
      get '/:username', to: 'users#profile', as: :profile
    end
  end
end
